#!/bin/bash

export KAFKA_ADDRESS="172.18.0.1"

export MICROMUSIC_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd )"
export MICROUTILS_DIR=${MICROMUSIC_ROOT}/micro_utilities/
