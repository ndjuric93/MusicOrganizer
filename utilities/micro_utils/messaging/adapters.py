from confluent_kafka import Producer as KafkaProducer
from confluent_kafka import Consumer as KafkaConsumer
from confluent_kafka import KafkaError, TopicPartition


class TopicProducer:

    def __init__(self, config, topic):
        self.producer = KafkaProducer(config)
        self.topic = topic

    @property
    def topics(self):
        return self.producer.list_topics()

    def produce(self, message):
        self.producer.poll(0)
        self.producer.produce(topic=self.topic, value=message, callback=self.__delivery_callback)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.producer.flush()

    @staticmethod
    def __delivery_callback(err, msg):
        if err:
            print('%% Message failed delivery: %s\n' % err)
        else:
            print('%% Message delivered to %s [%d] @ %o\n'
                  % (msg.topic(), msg.partition(), msg.offset()))


class TopicConsumer:

    def __init__(self, config, topics=[]):
        self.consumer = KafkaConsumer(config)
        self.topics = topics

    def __enter__(self):
        self.consumer.subscribe(self.topics)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.consumer.close()

    def consume(self):
        while True:
            msg = self.consumer.poll()
            if msg is None:
                continue
            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    continue
                else:
                    print(msg.error())
                    break
            return msg.value()
