from micro_playlist.app import db


class Playlist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    path = db.Column(db.String)
