""" Flask application """
import os
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from flask_jwt_simple import JWTManager

from micro_playlist.resources.playlist import api as playlist

db = SQLAlchemy()
ma = Marshmallow()
jwt = JWTManager()

_SERVER_CONFIG = {
    'host': os.environ['SERVER_ADDRESS'] or '127.0.0.1',
    'port': os.environ['SERVER_PORT']
}

SERVICE_NAME = 'MicroPlaylist'

if __name__ == '__main__':
    app = create_app(name=SERVICE_NAME, namespaces=[playlist], db=db, ma=ma,
                     jwt=jwt, kwargs=_SERVER_CONFIG)
    app.run(host=_SERVER_CONFIG['host'], port=_SERVER_CONFIG['port'])
