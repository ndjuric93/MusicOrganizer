from flask_restplus import Resource, Namespace, fields

from micro_utils.messaging.adapters import TopicConsumer

api = Namespace('')

_KAFKA_CONFIG = {
    'bootstrap.servers': 'localhost:9092'
}

TOPIC = 'player'


@api.route('/player')
class Playlist(Resource):

    def __init__(self, *args, **kwargs):
        super(Resource, self).__init__(*args, **kwargs)

    def get(self):
        return self.playlist
