from flask_restplus import Api

from .playlist import api as playlist

api = Api(title='Playlist')
api.add_namespace(playlist)
