from micro_playlist.app import ma


class PlaylistSchema(ma.ModelSchema):
    class Meta:
        fields = ('id',)


playlist_schema = PlaylistSchema(many=True)
