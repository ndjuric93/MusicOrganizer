from flask import Response
from flask_restplus import Resource, Namespace
from flask_jwt_simple import jwt_required, get_jwt_identity

from micro_utils.messaging.kafka import consume_messages

api = Namespace('')

DATA_TO_STREAM = 1024


@api.route('/play')
class Player(Resource):

    @jwt_required
    def get(self):
        conf = {'bootstrap.servers': '0.0.0.0:9092', 'group.id': get_jwt_identity(),
                'auto.offset.reset': 'earliest'}
        topic = ['playlist_' + str(get_jwt_identity())]
        path = consume_messages(topics=topic, config=conf)
        print(path)
        return Response(Player.play_song(path), mimetype="audio/mp3", status=200)

    @staticmethod
    def play_song(path):
        with open(path, "rb") as song:
            data = song.read(DATA_TO_STREAM)
            while data:
                yield data
                data = song.read(DATA_TO_STREAM)
