from flask_restplus import Api

from .player import api as player

api = Api(title='Player')
api.add_namespace(player)
