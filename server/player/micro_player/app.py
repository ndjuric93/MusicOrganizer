""" Flask application """
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from flask_jwt_simple import JWTManager

from micro_player import create_app
from micro_player.config import SERVER_CONFIG

db = SQLAlchemy()
ma = Marshmallow()
jwt = JWTManager()

SERVICE_NAME = 'MicroPlaylist'

if __name__=='__main__':
    app = create_app(name=SERVICE_NAME, **SERVER_CONFIG)
    app.run(host='0.0.0.0', port='5003', debug=True)
