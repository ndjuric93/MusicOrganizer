from micro_utils.messaging.adapters import TopicConsumer

class Playlist:

    playlist = {}

    def __init__(self, id):
        self.id = id

    @property
    def tracks(self):
        # TODO: Redis?
        return self.playlist[self.id]

