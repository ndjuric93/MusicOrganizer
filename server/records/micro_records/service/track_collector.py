import os

import eyed3 as id_reader

from micro_utils.db.utils import get_or_create

from micro_records.config import MUSIC_ROOT
from micro_records.models import Artist, Album, Track
from micro_records import db

MUSIC_ROOT = '/home/nemanja/Downloads/VoodooLoungeUncut'


def collect_tracks():
    collected_tracks = {}
    for path in os.listdir(MUSIC_ROOT):
        if path.endswith('.mp3'):
            path = os.path.join(MUSIC_ROOT, path)
            track = id_reader.load(path).tag
            artist = add_artist(name=track.artist)
            album = add_album(artist_id=artist.id, name=track.album)
            track = add_track(album_id=album.id, name=track.title, track_number=track.track_num[0], path=path)
            collected_tracks[artist.name] = {album.name: track.name}
    return collected_tracks



def add_artist(**kwargs):
    return get_or_create(db.session, Artist, **kwargs)


def add_album(**kwargs):
    return get_or_create(db.session, Album, **kwargs)


def add_track(**kwargs):
    track = Track(**kwargs)
    db.session.add(track)
    db.session.commit()
    return track
