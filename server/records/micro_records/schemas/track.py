from micro_records import ma

from micro_records.models import Track


class TrackSchema(ma.ModelSchema):
    class Meta:
        model = Track


track_schema = TrackSchema()
tracks_schema = TrackSchema(many=True)
