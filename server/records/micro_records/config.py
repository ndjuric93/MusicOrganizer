import os

SERVER_CONFIG = {
    'host': os.environ.get('SERVER_ADDRESS', '0.0.0.0'),
    'port': os.environ.get('SERVER_PORT', '5002')
}

DB_SETTINGS = {
    'user': os.environ.get('DB_USER', 'microrecords'),
    'name': 'records',
    'address': os.environ.get('DB_ADDRESS', 'localhost')
}

KAFKA_CONFIG = {
    'bootstrap.servers': '0.0.0.0:9092'
}

MUSIC_ROOT = os.environ.get('MUSIC_ROOT')
