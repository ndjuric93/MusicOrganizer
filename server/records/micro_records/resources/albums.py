from flask_restplus import Namespace, Resource

from flask_jwt_simple import jwt_required, get_jwt_identity

from micro_records.models import Album
from micro_records.schemas import albums_schema

api = Namespace('', description='TrackFetcher')


@api.route('/albums')
class Albums(Resource):

    @jwt_required
    def get(self):
        albums = albums_schema.dump(Album.query.all()).data
        return {'albums': albums}
