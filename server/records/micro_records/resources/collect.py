import json

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from flask import jsonify
from flask_restplus import Namespace, Resource
from flask_jwt_simple import jwt_required

from micro_records.service.track_collector import collect_tracks

api = Namespace('', description='Track collector')


@api.route('/refresh_tracks')
class RefreshTracks(Resource):

    @jwt_required
    def post(self):
        try:
            tracks = collect_tracks()
            return jsonify({'Status': json.dumps(tracks)})
        except (IntegrityError, NoResultFound):
            return jsonify({'Status': 'Failed'})
