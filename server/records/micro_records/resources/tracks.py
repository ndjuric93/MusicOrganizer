from flask_restplus import Namespace, Resource

from flask import request, jsonify
from flask_jwt_simple import jwt_required, get_jwt_identity

from micro_utils.messaging.adapters import TopicProducer

from micro_records.models import Track
from micro_records.schemas import tracks_schema

api = Namespace('', description='Tracks Lister')


@api.route('/tracks/<int:album>')
class Tracks(Resource):

    @jwt_required
    def get(self, album):
        tracks = Track.query.filter(Track.album_id == album)
        schema = tracks_schema.dump(tracks).data
        print(schema)
        return {'tracks': schema}

    @jwt_required
    def post(self, album):
        track = Track.query.filter(Track.id == album).first()
        topic = 'playlist_' + str(get_jwt_identity())
        config = {'bootstrap.servers': '0.0.0.0:9092'}
        with TopicProducer(config=config, topic=topic) as producer:
            producer.produce(message=track.path)
        return {'Status': 'Track ' + track.name + ' added'}, 200
