''' Flask application '''
from gevent import monkey
monkey.patch_all()

from micro_gateway import create_app

from micro_gateway.config import SERVER_CONFIG

if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port='5000', debug=True)
