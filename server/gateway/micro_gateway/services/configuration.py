""" Gateway configuration will be set up in ini format """
from configparser import ConfigParser


class ConfigurationParser(object):

    def __init__(self, path, *args, **kwargs):
        super(ConfigurationParser, self).__init__(*args, **kwargs)
        self.parser = ConfigParser()
        self.parser.read(path)
        self.routing = self.parse_routes()
        self.joins = self.parse_joins()

    def parse_routes(self):
        return self._parse('Routes')

    def parse_joins(self):
        return self._parse('Joins')

    def _parse(self, section):
        if section in self.parser.sections():
            return {
                key: [val for val in value.split()] for key, value in self.parser[section].items()
            }
        return {}
