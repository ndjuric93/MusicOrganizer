from flask_restplus import Api

from .root import api as root
from .gateway import api as gateway

api = Api(title='Gateway')
api.add_namespace(root)
api.add_namespace(gateway)
