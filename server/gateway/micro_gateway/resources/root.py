from flask_restplus import Resource, Namespace
from flask_jwt_simple import jwt_required

api = Namespace('test')

@api.route('/')
class Root(Resource):

    def get(self):
        return {'Status': 'Success'}, 200
