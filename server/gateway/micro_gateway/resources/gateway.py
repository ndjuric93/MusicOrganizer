import os
import grequests

from flask import request
from flask_restplus import Namespace, Resource

from micro_gateway.services.configuration import ConfigurationParser
from micro_utils.service_registry import ServiceRegistry

api = Namespace('')


@api.route('/api/')
@api.route('/api/<path:url_path>')
class Gateway(Resource):

    _config = ConfigurationParser(os.environ.get('GATEWAY_CONFIG'))
    _routes = _config.routing
    _joins = _config.joins

    def post(self, url_path=''):
        return self._generate_request(url_path, grequests.post, api.payload, request.headers)

    def get(self, url_path=''):
        return self._generate_request(url_path, grequests.get, api.payload, request.headers)

    def _generate_request(self, url_path, method, params, headers):
        routes = self._get_available_routes(self._combine_routes(self._routes, url_path))
        responses = grequests.imap(method(u, json=params, headers=headers) for u in routes)
        gateway_response = {}
        for response in responses:
            gateway_response.update(response.json())
        return gateway_response

    @staticmethod
    def _combine_routes(routes, path):
        route_list = [path.split('/')[0]]  # TODO Gateway has to be redone so it works with urls better.
        if path in routes:
            route_list += routes[path]
        return route_list

    @staticmethod
    def _get_available_routes(requested_routes):
        with ServiceRegistry() as registry:
            available_routes = {
                service['address']: list(
                    set(requested_routes).intersection(service['routes'])
                ) for service in registry.services.values()
            }
        return ['http://' + address + '/' + route for address, routes in available_routes.items() for route in routes]
