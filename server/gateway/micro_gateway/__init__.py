import os

from flask import Flask
from flask_restplus import Api
from flask_jwt_simple import JWTManager

SERVICE_NAME = 'MicroGateway'

jwt = JWTManager()


def create_app():
    app = Flask(__name__)
    jwt.init_app(app=app)
    app.config['JWT_SECRET_KEY'] = os.environ.get('JWT_KEY', 'my-secret-key')
    api = Api(app=app)

    from micro_gateway.resources import api as gateway

    api.add_namespace(gateway)

    return app
