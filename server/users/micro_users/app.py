""" Flask application """
from micro_users import create_app
from micro_users.config import SERVER_CONFIG


SERVICE_NAME = 'MicroUser'

if __name__ == '__main__':
    app = create_app(name=SERVICE_NAME, **SERVER_CONFIG)
    app.run(host='0.0.0.0', port='5001', debug=True)
