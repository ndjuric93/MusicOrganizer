import os

SERVER_CONFIG = {
    'host': os.environ.get('SERVER_ADDRESS', '0.0.0.0'),
    'port': os.environ.get('SERVER_PORT', '5001')
}

DB_SETTINGS = {
    'user': os.environ.get('DB_USER', 'microusers'),
    'name': 'users',
    'address': os.environ.get('DB_ADDRESS', 'localhost')
}
